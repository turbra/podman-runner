# podman-runner
# How to configure a gitlab-runner to work with podman

### 1. Download the latest GitLab Runner binary:

`sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"`

### 2. Give the binary executable permissions:

`sudo chmod +x /usr/local/bin/gitlab-runner`

### 3. Create a new system user or choose an existing user to run the GitLab Runner:
`sudo useradd --comment 'New GitLab Runner User' --create-home new_runner_user --shell /bin/bash`
* Replace `new_runner_user` with the desired username. If you want to use an existing user, skip this step.
  
### 4. Change ownership of the GitLab Runner configuration directory:
`sudo chown -R new_runner_user:new_runner_user /etc/gitlab-runner`
* Replace new_runner_user with the desired username.
  
### 5. Install the GitLab Runner service with the new user:
`sudo gitlab-runner install --user=new_runner_user --working-directory=/home/new_runner_user`
* Replace new_runner_user with the desired username and adjust the working directory if needed.

### 6. Start the GitLab Runner service:
`sudo gitlab-runner start`
